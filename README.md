### Teacode zadanie

**Run this commands to successfully start an application locally:**
> 1. `git clone https://gitlab.com/DawidRubch/teacodezadanie.git`
> 2. `yarn`
> 3. `yarn start`
> 4. Open [localhost:3000](http://localhost:3000) in your browser

**Or instead you can view it live [here](https://main.d2b04vvt3sl4uy.amplifyapp.com/) hosted on **AWS amplify**.**
