/**
 * Sorts based on last name
 */
export const sortUsers = (users: Contact[]) =>
  users.sort((a, b) => (a.last_name > b.last_name ? 1 : -1));
