/**
 * Filters users array by the start of first_name and last_name
 */
export const filterUsersBySearchResults = (value: string, users: Contact[]) =>
  users.filter(
    (user) =>
      verifyStringStartsWith(user.first_name, value) ||
      verifyStringStartsWith(user.last_name, value)
  );

const verifyStringStartsWith = (str: string, value: string) => {
  return str.toLocaleLowerCase().startsWith(value);
};
