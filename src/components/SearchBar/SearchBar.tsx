import { FC } from "react";
import { Input } from "antd";
import "./SearchBar.css";

interface SearchBarProps {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  value: string;
}

export const SearchBar: FC<SearchBarProps> = ({ onChange, value }) => {
  return <Input.Search value={value} onChange={onChange} className="search-bar" size="large" />;
};
