import { FC } from "react";
import { Checkbox } from "../Checkbox/Checkbox";
import "./ContactTile.css";

interface ContactTileProps {
  contact: Contact;
  checkBoxValue: boolean;
  onCheckBoxChange: () => void;
  style: React.CSSProperties;
}

const placeHolderAvatar =
  "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";

export const ContactTile: FC<ContactTileProps> = ({
  contact,
  checkBoxValue,
  onCheckBoxChange,
  style,
}) => {
  const { avatar, first_name, last_name } = contact;
  return (
    <div className="contact-tile" style={style}>
      <a onClick={() => onCheckBoxChange()}>
        <img src={avatar || placeHolderAvatar} />
        <span>
          {first_name} {last_name}
        </span>
        <Checkbox value={checkBoxValue} onChange={onCheckBoxChange} />
      </a>
    </div>
  );
};
