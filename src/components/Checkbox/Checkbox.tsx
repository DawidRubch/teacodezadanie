import { Checkbox as AntDCheckbox } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox/Checkbox";
import { FC } from "react";

interface CheckboxProps {
  onChange: (e: CheckboxChangeEvent) => void;
  value: boolean;
}

export const Checkbox: FC<CheckboxProps> = ({ onChange, value }) => {
  return <AntDCheckbox onChange={onChange} checked={value} />;
};
