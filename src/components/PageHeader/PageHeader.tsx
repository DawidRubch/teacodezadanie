import { PageHeader as AntDPageHeader } from "antd";
import "./PageHeader.css";

export const PageHeader = () => {
  return <AntDPageHeader className="site-page-header" title="Contacts" />;
};
