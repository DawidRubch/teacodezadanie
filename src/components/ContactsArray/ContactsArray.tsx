import { FC, useState } from "react";
import { ContactTile } from "../ContactTile/ContactTile";
import { List, ListRowRenderer } from "react-virtualized";

interface ContactsArrayProps {
  contacts: Contact[];
}

export const ContactsArray: FC<ContactsArrayProps> = ({ contacts }) => {
  const [checkedUsers, setCheckedUsers] = useState<number[]>([]);

  //Function adds or removes user from array of checked users
  const handleCheckboxChange = (contact: Contact) => {
    if (checkedUsers.includes(contact.id)) {
      setCheckedUsers(checkedUsers.filter((id) => id !== contact.id));
    } else {
      setCheckedUsers([...checkedUsers, contact.id]);
    }

    console.log(checkedUsers);
  };

  const renderRow: ListRowRenderer = ({ index, style }) => {
    const contact = contacts[index];
    return (
      <ContactTile
        style={style}
        key={contact.id}
        contact={contact}
        checkBoxValue={checkedUsers.includes(contact.id)}
        onCheckBoxChange={() => handleCheckboxChange(contact)}
      />
    );
  };
  return (
    <List
      rowRenderer={renderRow}
      width={3000}
      height={3000}
      rowCount={contacts.length}
      rowHeight={100}
    />
  );
};
