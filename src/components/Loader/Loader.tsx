import ReactLoader from "react-loader-spinner";
import "./Loader.css";

export const Loader = () => {
  return (
    <div className="loader">
      <ReactLoader
        type="Oval"
        color="#00BFFF"
        height={100}
        width={100}
        timeout={3000}
      />
    </div>
  );
};
