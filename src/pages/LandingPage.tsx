import { ChangeEvent, useCallback, useMemo, useState } from "react";
import { ContactsArray, Loader, PageHeader, SearchBar } from "../components";
import { useUsers } from "../hooks";
import { filterUsersBySearchResults } from "../utils";
import { ErrorPage } from "./ErrorPage";

export const LandingPage = () => {
  const [value, onChange] = useState("");
  const { users, isLoading, isError } = useUsers();

  const onSearch = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value),
    []
  );
  const filteredUsers = useMemo(
    () => filterUsersBySearchResults(value, users),
    [value, users]
  );

  if (isLoading) {
    return <Loader />;
  }

  if (isError) {
    return <ErrorPage />;
  }

  return (
    <div>
      <PageHeader />
      <SearchBar value={value} onChange={onSearch} />
      <ContactsArray contacts={filteredUsers} />
    </div>
  );
};
