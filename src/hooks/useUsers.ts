import axios from "axios";
import { useEffect, useMemo, useState } from "react";
import { sortUsers } from "../utils";

type ResponseData = Contact[];

const userEndpoint =
  "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

const fetchUsers = async (): Promise<ResponseData> => {
  const response = await axios.get(userEndpoint);
  return response.data;
};

export const useUsers = () => {
  const [users, setUsers] = useState<ResponseData>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchUsers();
        setUsers(data);
        setIsLoading(false);
      } catch (error) {
        setIsError(true);
      }
    };
    fetchData();
  }, []);
  

  const sortedUsers = useMemo(() => sortUsers(users), [users]);

  return { users: sortedUsers, isLoading, isError };
};
